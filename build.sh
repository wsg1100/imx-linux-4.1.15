#!/bin/bash 
make distclean
make imx_v7_defconfig
make zImage -j$(nproc)
make dtbs
make modules -j$(nproc)
