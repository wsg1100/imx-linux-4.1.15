/******************************************************************************
 *
 *  $Id$
 *
 *  Copyright (C) 2006-2008  Florian Pose, Ingenieurgemeinschaft IgH
 *
 *  This file is part of the IgH EtherCAT Master.
 *
 *  The IgH EtherCAT Master is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 2, as
 *  published by the Free Software Foundation.
 *
 *  The IgH EtherCAT Master is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *  Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with the IgH EtherCAT Master; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *
 *****************************************************************************/

/** \file
 *
 * EtherCAT interface for EtherCAT device drivers.
 *
 * \defgroup DeviceInterface EtherCAT Device Interface
 *
 * Master interface for EtherCAT-capable network device drivers. Through the
 * EtherCAT device interface, EtherCAT-capable network device drivers are able
 * to connect their device(s) to the master, pass received frames and notify
 * the master about status changes. The master on his part, can send his
 * frames through connected devices.
 */

/*****************************************************************************/

#ifndef __ECDEV_H__
#define __ECDEV_H__

#include "../globals.h"

#ifdef EC_RT_NET
/* RTnet */
#include "rtnet_port.h"
#include "rtskb.h"

// RTNET wrappers
/*#define kmalloc(a,b) rtdm_malloc(a)
#define vmalloc(a) rtdm_malloc(a)
#define kfree(a) rtdm_free(a)
#define vfree(a) rtdm_free(a)
#define skb_reserve(a,b) rtskb_reserve(a,b)
#define dev_kfree_skb kfree_rtskb
#define dev_alloc_skb alloc_rtskb
#define skb_push rtskb_push
#define net_device rtnet_device
#define sk_buff rtskb
*/
// ----------------------
#else
#include <linux/netdevice.h>
#endif

/*****************************************************************************/

struct ec_device;
typedef struct ec_device ec_device_t; /**< \see ec_device */

/** Device poll function type.
 */
#ifdef EC_RT_NET
 typedef void (*ec_pollfunc_t)(struct rtnet_device *);

/******************************************************************************
 * Offering/withdrawal functions
 *****************************************************************************/

ec_device_t *ecdev_offer(struct rtnet_device *net_dev, ec_pollfunc_t poll,
        struct module *module);
 #else
typedef void (*ec_pollfunc_t)(struct net_device *);

/******************************************************************************
 * Offering/withdrawal functions
 *****************************************************************************/

ec_device_t *ecdev_offer(struct net_device *net_dev, ec_pollfunc_t poll,
        struct module *module);
#endif

void ecdev_withdraw(ec_device_t *device);

/******************************************************************************
 * Device methods
 *****************************************************************************/

int ecdev_open(ec_device_t *device);
void ecdev_close(ec_device_t *device);
void ecdev_receive(ec_device_t *device, const void *data, size_t size);
void ecdev_set_link(ec_device_t *device, uint8_t state);
uint8_t ecdev_get_link(const ec_device_t *device);

/*****************************************************************************/

#endif
