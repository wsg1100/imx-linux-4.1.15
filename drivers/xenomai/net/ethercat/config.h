/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Debug interfaces enabled */
#undef EC_DEBUG_IF
#ifdef CONFIG_EC_DEBUG_IF
#define EC_DEBUG_IF 1
#endif

/* Debug ring enabled */
#undef EC_DEBUG_RING
#ifdef CONFIG_EC_DEBUG_RING
#define EC_DEBUG_RING 1
#endif 

/* EoE support enabled */
#ifdef CONFIG_EC_EOE
#define EC_EOE 1
#else
#undef EC_EOE
#endif

/* Use CPU timestamp counter */
#undef EC_HAVE_CYCLES
#ifdef CONFIG_EC_HAVE_CYCLES
#define EC_HAVE_CYCLES 1
#endif

/* Use vendor id / product code wildcards */
#undef EC_IDENT_WILDCARDS
#ifdef CONFIG_EC_IDENT_WILDCARDS
#define EC_IDENT_WILDCARDS 1
#endif

/* Use loop control registers */
#undef EC_LOOP_CONTROL
#ifdef CONFIG_EC_LOOP_CONTROL
#define EC_LOOP_CONTROL 1
#endif

/* Max. number of Ethernet devices per master */

#ifdef CONFIG_EC_MAX_NUM_DEVICES
#define EC_MAX_NUM_DEVICES (CONFIG_EC_MAX_NUM_DEVICES)
#else
#define EC_MAX_NUM_DEVICES 1
#endif

/* Allow quick return to OP from SAFEOP+PD_watchdog. */
#undef EC_QUICK_OP
#ifdef CONFIG_EC_QUICK_OP
#define EC_QUICK_OP 1
#endif

/* Force refclk to OP */
#undef EC_REFCLKOP
#ifdef CONFIG_EC_REFCLKOP
#define EC_REFCLKOP 1
#endif

/* Read alias adresses from register */
#undef EC_REGALIAS
#ifdef CONFIG_EC_REGALIAS
#define EC_REGALIAS 1
#endif

/* RTDM interface enabled */
#define EC_RTDM 1

/* RTDM rtnet interface enabled */
#define EC_RT_NET 1

/* Output to syslog in RT context */
#undef EC_RT_SYSLOG
#ifdef CONFIG_EC_RT_SYSLOG
#define EC_RT_SYSLOG 1
#endif


/* Assign SII to PDI */
#undef EC_SII_ASSIGN
#ifdef CONFIG_EC_SII_ASSIGN
#define EC_SII_ASSIGN 1
#endif

/* Cache the SII images to improve slave re-scan performance in operational
   state. */
#undef EC_SII_CACHE
#ifdef CONFIG_EC_SII_CACHE
#define EC_SII_CACHE 1
#endif

/* Direct SII firmware base path */
#undef EC_SII_DIR
#ifdef CONFIG_EC_SII_DIR
#define EC_SII_DIR 1
#endif

/* Allow locally loadable SII */
#undef EC_SII_OVERRIDE
#ifdef CONFIG_EC_SII_OVERRIDE
#define EC_SII_OVERRIDE 1
#endif

/* Use hrtimer for scheduling */
#undef EC_USE_HRTIMER
#ifdef CONFIG_EC_USE_HRTIMER
#define EC_USE_HRTIMER 1
#endif

/* Use rtmutex for synchronization */
#undef EC_USE_RTMUTEX
#ifdef CONFIG_EC_USE_RTMUTEX
#define EC_USE_RTMUTEX 1
#endif

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#define LT_OBJDIR ".libs/"

/* Name of package */
#define PACKAGE "ethercat"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "fp@igh-essen.com"

/* Define to the full name of this package. */
#define PACKAGE_NAME "ethercat"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "ethercat 1.5.2"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "ethercat"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.5.2"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "1.5.2"
